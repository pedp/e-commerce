package com.econorma.main;

import java.util.List;
import java.util.Map;

import org.jdesktop.application.SingleFrameApplication;

import com.econorma.dao.DAO;
import com.econorma.dao.DatabaseManager;
import com.econorma.data.Ordine;
import com.econorma.data.Status;
import com.econorma.logic.ApplicationManager;
import com.econorma.properties.AppProperties;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class Application extends SingleFrameApplication {

	private static final Logger logger = Logger.getLogger(Application.class);
	private static final String TAG = "Application";

	private DatabaseManager databaseManager;
	private ApplicationManager appManager;
	private DAO dao;

	public void init() {

		try {
			databaseManager = DatabaseManager.getInstance();
			dao = DAO.create(databaseManager);
			databaseManager.init();
			dao.init();
		} catch (Exception e) {
			logger.error(TAG, e);
		}

		try {
			Map<String, String> map = AppProperties.getInstance().load();
			String key = map.get("key");
			String secret = map.get("secret");
			int subbedDays = Integer.parseInt(map.get("subbed_days"));

			appManager = new ApplicationManager(dao, key, secret, subbedDays);
			appManager.run();

			List<Ordine> ordini = dao.readOrdini(subbedDays);

			for (Ordine ordine : ordini) {
				if (ordine.getStatoConferma().equals(Testo.SI)){
					continue;
				}
				logger.info(TAG, "******************** NON FA Aggiornamento ordine e-commerce in corso: " + ordine.getNumero() +  "|" + ordine.getNumeroStoreden() + "|"  + Status.DELIVERED.toString()+ "*****************************");
				String riferimento = ordine.getNumeroStoreden().toUpperCase().replace("E-COMMERCE:", "");
//				appManager.updateOrder(Integer.parseInt(riferimento), Status.DELIVERED);
				dao.updateOrdine(ordine);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		}

		exit();

	}

	@Override
	protected void initialize(String[] args) {
		super.initialize(args);
	}

	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

	public DatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	public void setDatabaseManager(DatabaseManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	@Override
	protected void shutdown() {
		System.exit(1);
	}

	@Override
	protected void startup() {
		init();
	}

}
