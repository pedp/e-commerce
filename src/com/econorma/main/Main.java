package com.econorma.main;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.econorma.dao.DAO;
import com.econorma.dao.DatabaseManager;
import com.econorma.data.Ordine;
import com.econorma.data.Status;
import com.econorma.logic.ApplicationManager;
import com.econorma.properties.AppProperties;
import com.econorma.util.Logger;
 

public class Main {
	
	private static final String TAG = Main.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(Main.class);
	
	public static void main(String[] args) throws Exception {
	 
			File f = new File("application.properties");
			if(!f.exists()) { 
				AppProperties.getInstance().create();
				logger.info(TAG, "File properties creato");
			}
			
			Application.launch(Application.class, args);
			
			
		 
	}

}
