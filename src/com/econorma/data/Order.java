package com.econorma.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Order {
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	private int id;
	private String currency;
	private Double total;
	private Double total_tax;
	private String order_key;
	private String date_created;
	private String date_created_gmt;
	private String status;
	private String customer_note;
	private int customer_id;
	private Billing billing;
	private Shipping shipping;
	private String payment_method;
	private String payment_method_title;
	private List<Item> line_items;
	private List<Meta> meta_data;
	private List<ShippingLines> shipping_lines;
	private List<FeeLines> fee_lines;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCurrency() {
		return currency;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getTotal_tax() {
		return total_tax;
	}
	public void setTotal_tax(Double total_tax) {
		this.total_tax = total_tax;
	}
	public String getOrder_key() {
		return order_key;
	}
	public void setOrder_key(String order_key) {
		this.order_key = order_key;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public Shipping getShipping() {
		return shipping;
	}
	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}
	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	public String getPayment_method_title() {
		return payment_method_title;
	}
	public void setPayment_method_title(String payment_method_title) {
		this.payment_method_title = payment_method_title;
	}
	public List<Item> getLine_items() {
		return line_items;
	}
	public void setLine_items(List<Item> line_items) {
		this.line_items = line_items;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
	public String getDate_created_gmt() {
		return date_created_gmt;
	}
	public void setDate_created_gmt(String date_created_gmt) {
		this.date_created_gmt = date_created_gmt;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public List<Meta> getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(List<Meta> meta_data) {
		this.meta_data = meta_data;
	}
	public String getCustomer_note() {
		return customer_note;
	}
	public void setCustomer_note(String customer_note) {
		this.customer_note = customer_note;
	}
	public List<ShippingLines> getShipping_lines() {
		return shipping_lines;
	}
	public void setShipping_lines(List<ShippingLines> shipping_lines) {
		this.shipping_lines = shipping_lines;
	}
	public List<FeeLines> getFee_lines() {
		return fee_lines;
	}
	public void setFee_lines(List<FeeLines> fee_lines) {
		this.fee_lines = fee_lines;
	}
	
}
