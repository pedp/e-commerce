package com.econorma.data;

import java.util.Date;

public class Ordine {
	
	String numero;
	Date data;
	String cliente;
	String giro;
	String stato;
	String numeroStoreden;
	String statoConferma;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public String getNumeroStoreden() {
		return numeroStoreden;
	}
	public void setNumeroStoreden(String numeroStoreden) {
		this.numeroStoreden = numeroStoreden;
	}
	public String getStatoConferma() {
		return statoConferma;
	}
	public void setStatoConferma(String statoConferma) {
		this.statoConferma = statoConferma;
	}
	
}
