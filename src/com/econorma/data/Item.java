package com.econorma.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Item {
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	private int id;
	private String name;
	private int product_id;
	private int variation_id;
	private String sku;
	private Double quantity;
	private Double subtotal;
	private Double subtotal_tax;
	private Double total;
	private Double price;
	private Double total_tax;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name.toUpperCase();
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	public Double getSubtotal_tax() {
		return subtotal_tax;
	}
	public void setSubtotal_tax(Double subtotal_tax) {
		this.subtotal_tax = subtotal_tax;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getTotal_tax() {
		return total_tax;
	}
	public void setTotal_tax(Double total_tax) {
		this.total_tax = total_tax;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getVariation_id() {
		return variation_id;
	}
	public void setVariation_id(int variation_id) {
		this.variation_id = variation_id;
	}
	
}
