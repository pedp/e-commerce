package com.econorma.data;

public enum Status {
	
    PROCESSING ("processing"),
    COMPLETED ("completed"),
    PENDING ("pending"),
    PROGRESS ("in-preparazione"),
	TRASH ("trash"),
	FAILED ("failed"),
	CANCELLED ("cancelled"),
	DELIVERED ("spedito");
	
    private final String name;       

    private Status(String s) {
        name = s;
    }
 
    public String toString() {
       return this.name;
    }
}
