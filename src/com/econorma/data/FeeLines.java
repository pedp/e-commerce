package com.econorma.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class FeeLines {
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	private int id;
	private String name;
	private String tax_class;
	private String tax_status;
	private Double total;
	private Double total_tax;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTax_class() {
		return tax_class;
	}
	public void setTax_class(String tax_class) {
		this.tax_class = tax_class;
	}
	public String getTax_status() {
		return tax_status;
	}
	public void setTax_status(String tax_status) {
		this.tax_status = tax_status;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getTotal_tax() {
		return total_tax;
	}
	public void setTotal_tax(Double total_tax) {
		this.total_tax = total_tax;
	}
	 
	
}
