package com.econorma.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Shipping {
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	private String first_name;
	private String last_name;
	private String company;
	private String address_1;
	private String city;
	private String state;
	private String postcode;
	private String country;
	
	
	public String getFirst_name() {
		return first_name.toUpperCase();
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name.toUpperCase();
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getCompany() {
		return company.toUpperCase();
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddress_1() {
		return address_1.toUpperCase();
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getCity() {
		return city.toUpperCase();
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state.toUpperCase();
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostcode() {
		return postcode.toUpperCase();
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getCountry() {
		return country.toUpperCase();
	}
	public void setCountry(String country) {
		this.country = country;
	}
 	
}
