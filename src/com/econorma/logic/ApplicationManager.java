package com.econorma.logic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import com.econorma.dao.DAO;
import com.econorma.data.FeeLines;
import com.econorma.data.Item;
import com.econorma.data.Meta;
import com.econorma.data.Order;
import com.econorma.data.ShippingLines;
import com.econorma.data.Status;
import com.econorma.util.Logger;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.icoderman.woocommerce.ApiVersionType;
import com.icoderman.woocommerce.EndpointBaseType;
import com.icoderman.woocommerce.WooCommerce;
import com.icoderman.woocommerce.WooCommerceAPI;
import com.icoderman.woocommerce.oauth.OAuthConfig;
import com.icoderman.woocommerce.oauth.OAuthSignature;

public class ApplicationManager {

	private DAO dao;
	private String key;
	private String secret;
	private int subbedDays;
	//	private String baseUrl = "https://www.blockcheese.it";
	private String baseUrl = "https://www.fromago.it";
	private WooCommerce wooCommerce;
	private OAuthConfig config;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static final String TAG = ApplicationManager.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(ApplicationManager.class);

	public ApplicationManager(DAO dao, String key, String secret, int subbedDays) {
		this.dao = dao;
		this.key = key;
		this.secret = secret;
		this.subbedDays = subbedDays;
	}

	public void run() throws Exception {
		create();
		List<Order> orders = getOrders();

		if (orders.size()>0) {
			logger.info(TAG, "Order size: " + orders.size());
			CsvCreator creatorCsv = new CsvCreator(orders);
			creatorCsv.execute();	
		}
		

//		for (Order o : orders) {
			//			updateOrder(o.getId(), Status.PENDING);
			//			updateOrder(o.getId(), Status.PROGRESS);
//		}

	}

	public void create() {
		config = new OAuthConfig(baseUrl, key, secret);
		wooCommerce = new WooCommerceAPI(config, ApiVersionType.V3);
	}

	public List<Order> getOrders() {
		List<Order> orders = new ArrayList<Order>();

		try {

			//			logger.info(TAG, "---------------BEGIN PRODUCTS------------------");
			//			List products = wooCommerce.getAll(EndpointBaseType.PRODUCTS.getValue());
			//			for (Object p : products) {
			//				ObjectMapper objectMapper = new ObjectMapper();
			//				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			//				String jsonStr = objectMapper.writeValueAsString(p);
			//				Product product = objectMapper.readValue(jsonStr, Product.class);
			//				logger.info(TAG, "Product Info: " + product.getSku() + "|" + product.getId() + "|" + product.getName()
			//				+ "|" + product.getDate_created() + "|" + product.getPrice());
			//			}
			//			logger.info(TAG, "---------------END PRODUCTS------------------");

			Map<String, String> params = new HashMap<>();
			params.put("per_page", "100");
			params.put("offset", "0");
			params.put("after", OAuthSignature.percentEncode(getFromDate()));

			List eorders = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), params);
			for (Object o : eorders) {
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonStr = objectMapper.writeValueAsString(o);
				Order order = objectMapper.readValue(jsonStr, Order.class);
				String riferimento = "E-COMMERCE:" + String.valueOf(order.getId());
				logger.info(TAG, "Order: " + order.getOrder_key() + "|" + order.getDate_created() + "|" + order.getCustomer_id() + "|" + order.getStatus()+ "|" + riferimento);
				if (!order.getStatus().equals(Status.PROCESSING.toString())){
					continue;
				}
				if(dao.findOrdineByRiferimento(riferimento, subbedDays)){
					continue;
				}
				logger.info(TAG, "JSON: " + jsonStr);
				logger.info(TAG, "---------------BEGIN NEW ORDER------------------");
				logger.info(TAG, "Order Info: " + order.getId() + "|" + order.getOrder_key() + "|" + order.getCurrency()
				+ "|" + order.getTotal() + "|" + order.getStatus() + "|" + order.getPayment_method()+ "|" + order.getPayment_method_title());
				logger.info(TAG,
						"Shipping Info: " + order.getShipping().getFirst_name() + "|"
								+ order.getShipping().getLast_name() + "|" + order.getShipping().getCity() + "|"
								+ order.getShipping().getAddress_1());
				logger.info(TAG,
						"Billing Info: " + order.getBilling().getFirst_name() + "|" + order.getBilling().getLast_name()
						+ "|" + order.getBilling().getCity() + "|" + order.getBilling().getAddress_1());

				List<FeeLines> feeLines = order.getFee_lines();
				for (FeeLines f : feeLines) {
					logger.info(TAG, "Fee Lines: " + f.getName() + "|" + f.getTotal());
				}

				List<ShippingLines> shippingLines = order.getShipping_lines();
				for (ShippingLines s : shippingLines) {
					logger.info(TAG, "Shipping Lines: " + s.getMethod_title() + "|" + s.getMethod_id() + "|" + s.getTotal());
				}

				List<Item> items = order.getLine_items();
				for (Item i : items) {
					logger.info(TAG, "Item Info: " + i.getProduct_id() + "|" + i.getVariation_id() + "|" + i.getSku()
					+ "|" + i.getName() + "|" + i.getQuantity() + "|" + i.getTotal());
				}

				List<Meta> metas = order.getMeta_data();
				for (Meta m : metas) {
					logger.info(TAG, "Meta Info: " + m.getId() + "|" + m.getKey() + "|" + m.getValue());
				}

				orders.add(order);
				logger.info(TAG, "---------------END NEW ORDER------------------");
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		}

		return orders;

	}

	public String getFromDate() {
		Date dateFrom = DateUtils.addDays(new Date(), -subbedDays);
		return sdf.format(dateFrom);
	}

	public boolean updateOrder(int id, Status status) {
		boolean result = false;

		logger.info(TAG, "Aggiornamento stato ordine id: " + id);
		try {
			Map<String, Object> params = new HashMap<>();
			// params.put("status", Status.COMPLETED.toString());
			params.put("status", status.toString());
			wooCommerce.update(EndpointBaseType.ORDERS.getValue(), id, params);
			result = true;
		} catch (Exception e) {
			result = false;
			logger.error(TAG, e);
		}

		return result;
	}

}
