package com.econorma.logic;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.time.DateUtils;

import com.econorma.data.Billing;
import com.econorma.data.FeeLines;
import com.econorma.data.Item;
import com.econorma.data.Meta;
import com.econorma.data.Order;
import com.econorma.data.Shipping;
import com.econorma.data.ShippingLines;
import com.econorma.data.Status;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class CsvCreator {

	private List<Order> orders = new ArrayList<Order>();
	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private SimpleDateFormat sdfNew = new SimpleDateFormat("dd/MM/yyyy");
	private File csvFile;
	private PrintWriter pw;
	private StringBuilder sb;
	private static final String TAG = CsvCreator.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(CsvCreator.class);

	public CsvCreator(List<Order> orders) throws Exception {
		this.orders = orders;
	}

	public void openCsv() throws Exception{
		csvFile = new File(Testo.ECOMMERCE + "_" +UUID.randomUUID() + ".csv");
		pw = new PrintWriter(csvFile);
		sb = new StringBuilder();
	}

	public void closeCsv(){
		pw.write(sb.toString());
		pw.close();
		logger.info(TAG, "CVS file created: " +  csvFile);
	}

	public void execute() throws Exception{
	
		for (Order order: orders){
			
			if (!order.getStatus().equals(Status.PROCESSING.toString())){
				continue;
			}
 
			logger.info(TAG, "Inizio creazione CSV");	
			List<Item> line_items = order.getLine_items();
			List<Meta> meta_data = order.getMeta_data();
			String idCustomer = null;
			for (Meta meta: meta_data){
				if(meta.getKey().toUpperCase().contains(Testo.BILLING_PIVA) && meta.getValue().trim().length()>0){
					idCustomer=meta.getValue();
					break;
				}
			}
				

			openCsv();

			for (Item item: line_items) {
				addToCsv(order, idCustomer, item);
			}

			closeCsv();
			logger.info(TAG, "Fine creazione CSV");
		}
	}

	public void addToCsv(Order order, String idCustomer, Item item){
		

		try {
			Date dateOrder = sdf.parse(order.getDate_created());
			String orderDate = sdfNew.format(dateOrder);
			Shipping shipping = order.getShipping();
			Billing billing = order.getBilling();
			List<ShippingLines> shippingLines = order.getShipping_lines();
			List<FeeLines> feeLines = order.getFee_lines();
			double price  = Math.round(item.getPrice()  * 100);
			price = price/100;
			
			sb.append(Testo.ECOMMERCE);
			sb.append(';');
			sb.append(shipping.getFirst_name()+" " + shipping.getLast_name());	
			sb.append(';');
			sb.append(order.getId());
			sb.append(';');
			sb.append(orderDate);
			sb.append(';');
			sb.append(getFromDate(dateOrder));
			sb.append(';'); 
//			sb.append(item.getProduct_id()+"_"+item.getVariation_id());
			sb.append(item.getSku());
			sb.append(';');
			sb.append(item.getName());
			sb.append(';');
			sb.append(item.getQuantity());
			sb.append(';');
			sb.append(price);
			sb.append(';');
			sb.append("");
			sb.append(';');
			if (idCustomer!=null){
				sb.append(idCustomer);
			} else {
				sb.append(order.getCustomer_id());	
			}
			sb.append(';');
			if (shipping.getCompany().length()>0){
				sb.append(shipping.getCompany());
				sb.append(';');
			} else {
				sb.append(shipping.getFirst_name()+" " + shipping.getLast_name());
				sb.append(';');	
			}
			sb.append(shipping.getAddress_1());
			sb.append(';');
			sb.append(shipping.getPostcode());
			sb.append(';');
			sb.append(shipping.getCity());
			sb.append(';');
			sb.append(shipping.getState());
			sb.append(';');
			sb.append(billing.getPhone());
			sb.append(';');
			sb.append(billing.getEmail());
			sb.append(';');
			sb.append(order.getCustomer_note());
			sb.append(';');
			sb.append(';');
			sb.append(';');
			if (feeLines.size()>0) {
				sb.append(feeLines.get(0).getName().toUpperCase());
				sb.append(';');
				sb.append(feeLines.get(0).getTotal());
				sb.append(';');
			} else {
				sb.append(';');
				sb.append(';');
			}
			if (shippingLines.size()>0) {
				sb.append(shippingLines.get(0).getMethod_title().toUpperCase());
				sb.append(';');
				sb.append(shippingLines.get(0).getTotal());
				sb.append(';');				
			} else {
				sb.append(';');
				sb.append(';');
			}
			sb.append('\n');

		} catch (Exception e) {
			logger.error(TAG, e);
		}

	}

	public String getFromDate(Date orderDate){
		Date dateFrom = DateUtils.addDays(orderDate,1);
		return sdfNew.format(dateFrom);
	}


}
