package com.econorma.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.econorma.data.Ordine;

 

public class DAO {
  
	public static final boolean USE_AS400 = true;
	private DatabaseManager databaseManager;

	public DAO(){}

	public void setDatabaseManager(DatabaseManager dm){
		this.databaseManager = dm;
	}
	
	public void init() {
		if (USE_AS400) {
			Connection conn = databaseManager.getConnection();
		 
			try {
				 

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}


	}
	
	public List<Ordine> readOrdini(int subbedDays) throws SQLException {
		Connection connection = null;
		List<Ordine> ordini = null;
		try {
			connection = databaseManager.getConnection();
			ordini = DAOAS400.readOrdini(connection, subbedDays);
		}catch (Exception e) {
			System.out.println(e);
		} 

		finally {
			silentClose(connection);
		}
		return ordini;
	}
	
	public boolean findOrdineByRiferimento(String riferimento, int subbedDays) throws SQLException {
		Connection connection = null;
	 
		try {
			connection = databaseManager.getConnection();
			return DAOAS400.findOrdineByRiferimento(connection, riferimento, subbedDays);
		}catch (Exception e) {
			System.out.println(e);
		} 

		finally {
			silentClose(connection);
		}
		return false;
	}
	
	public boolean updateOrdine(Ordine ordine) throws SQLException {
		Connection connection = null;
		boolean result = false;
		try {
			connection = databaseManager.getConnection();
			result = DAOAS400.updateOrdine(connection, ordine);
		}catch (Exception e) {
			result = false;
			System.out.println(e);
		} 

		finally {
			silentClose(connection);
		}
		return result;
	}

	 

	private static void silentClose(ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// ignore
		}
	}
	
	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}

	public static DAO create(DatabaseManager manager){
		DAO dao = new DAO();
		dao.setDatabaseManager(manager);
		return dao;
	}

}
