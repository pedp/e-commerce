package com.econorma.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import com.econorma.properties.AppProperties;

public class DatabaseManager {

	private static DatabaseManager manager;
	private static Map<String, String> properties; 

	public static synchronized DatabaseManager getInstance() {
		if (manager == null) {
			properties = AppProperties.getInstance().load();
			manager = new DatabaseManager();
		}
		return manager;
	}

	public boolean init() {
		try {
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {
			System.out.println(e);
			return false;
		}
		return true;
	}

	public Connection getConnection() {
		
		String libraries= ";libraries=GA_V01_O, QGPL, QTEMP, IT_BRO_PF, IT_BRO_PO, CO_BRO_PO, IT_BRO_F, IT_V02_O, BMS60L_DAT, ACK60L_OBJ, PLEX510, DA_BRO_F, DA_V02_O, KMS60L_DAT, KEYTOOL";
		
		Connection conn = null;
		try {
			String host_as400 =  (String) properties.get("host_as400");
			String user_as400 =  (String) properties.get("user_as400");
			String password_as400 =  (String) properties.get("password_as400");
			conn = DriverManager.getConnection("jdbc:as400://" + host_as400 + libraries, user_as400, password_as400);
//			 conn = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return conn;

	}

}
