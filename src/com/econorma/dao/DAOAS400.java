package com.econorma.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.econorma.data.Ordine;
import com.econorma.util.Logger;





public class DAOAS400 {
	private static final String TAG = DAOAS400.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(DAOAS400.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");	 
	private static final String LIBRARY = "IT_BRO_F";
//	private static final String LIBRARY = "IT_V02_F";
	private static final String ORDINI = LIBRARY + ".OTCLI00F";
	

	private static final String F_CLIENTE =  "OTCLSP";
	private static final String F_NUMERO =  "OTNORC";
	private static final String F_ANNO =  "OTAORC";
	private static final String F_MESE =  "OTMORC";
	private static final String F_GIORNO =  "OTGORC";
	private static final String F_STATO =  "OTFSOR" ;
	private static final String F_STATO_CONFERMA =  "OTFCOO" ;
	private static final String F_GIRO =  "OTCGIV" ;
	private static final String F_TIPO_ORDINE =  "OTTPAQ";
	private static final String F_RIFERIMENTO_ORDINE =  "OTRIFC";
	private static final String F_ANNULLO =  "OTNULL";
	

	public static List<Ordine> readOrdini(Connection connection, int subbedDays){

		try{

			PreparedStatement st =null; 

			String select = "SELECT " +
					F_CLIENTE +", " +
					F_NUMERO+", " +
					F_ANNO+", " +
					F_MESE+", " +
					F_GIORNO+", " +
					F_GIRO+", " +
					F_STATO+", " +
					F_TIPO_ORDINE+", " +
					F_RIFERIMENTO_ORDINE+",  " +
					F_STATO_CONFERMA+"  " +
					" FROM "+ ORDINI + 
					" WHERE " + F_STATO + "=" + "'S'" + " AND " + F_TIPO_ORDINE + "=" +  "'E'" + " AND " + F_RIFERIMENTO_ORDINE + " LIKE " + "'%E-COMMERCE%'" + 
					" AND " + F_ANNO + "* 10000 + "  + F_MESE + "*100 + " + F_GIORNO + " >= " +  getFromDate(subbedDays) +
					" AND " + F_ANNULLO + "<>" + "'A'";
					  
	
			logger.debug(TAG, select);
			
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			List<Ordine> ordini = new ArrayList<Ordine>();
			
			while(rs.next()){

				try {
					String cliente = rs.getString(1);
					String numero = rs.getString(2);
					String anno = rs.getString(3);
					String mese = StringUtils.leftPad(rs.getString(4), 2, "0");
					String giorno = StringUtils.leftPad(rs.getString(5), 2, "0");
					String giro = rs.getString(6);
					String stato = rs.getString(7);
					String tipo = rs.getString(8);
					String numeroStoreden = rs.getString(9);
					String statoConferma = rs.getString(10);
					
					logger.info(TAG, "Ordine trovato: " +  numeroStoreden + "|" + numero + "|" +  giorno+mese+anno + "|" + cliente + "|" + giro + "|" + numeroStoreden + "|" + stato + "|");
					
					Ordine ordine = new Ordine();
					ordine.setCliente(cliente);
					ordine.setNumero(numero);
					ordine.setData(sdf.parse(anno+mese+giorno));
					ordine.setStato(stato);
					ordine.setGiro(giro);
					ordine.setNumeroStoreden(numeroStoreden.trim());
					ordine.setStatoConferma(statoConferma);
					ordini.add(ordine);
					
				} catch (Exception e) {
					logger.error(TAG, e);
				}
				
			}

			rs.close();

			return ordini;

		}

		catch(SQLException e){
			System.out.println(e);
			return null;
		}
	}
	
	public static boolean updateOrdine(Connection connection, Ordine ordine) throws SQLException{

		boolean result = false;
		 
		String dataOrdine = sdf.format(ordine.getData());
		String anno = dataOrdine.substring(0, 4);
		String mese = StringUtils.leftPad(dataOrdine.substring(4, 6), 2, "0");
		String giorno = StringUtils.leftPad(dataOrdine.substring(6, 8), 2, "0");
		
		try{
			PreparedStatement st =null;

			String select = "UPDATE " +
					ORDINI +  
					" SET " + F_STATO_CONFERMA + " = " + "'" + "S" + "'" +
					" WHERE " + F_NUMERO + " = " + "'" + ordine.getNumero() + "'" +
					" AND " + F_GIORNO + " = " +  giorno +   
					" AND " + F_MESE + " = " + mese +   
					" AND " + F_ANNO + " = " + anno;  
			logger.info(TAG, select);
			 
			st = connection.prepareStatement(select);
			st.executeUpdate();
			result = true;
			
			st.close();
			return result;

		}catch (SQLException e) {
			System.out.println(e);
			result = false;
		}
		return result;
	}
 	 
	public static String getFromDate(int subbedDays) {
		Date dateFrom = DateUtils.addDays(new Date(), -subbedDays);
		return sdf.format(dateFrom);
	}
	
	public static boolean findOrdineByRiferimento(Connection connection, String riferimento, int subbedDays){

		boolean found = false;
		
		try{

			PreparedStatement st =null; 

//			String select = "SELECT " +
//					F_CLIENTE +", " +
//					F_NUMERO+", " +
//					F_ANNO+", " +
//					F_MESE+", " +
//					F_GIORNO+", " +
//					F_GIRO+", " +
//					F_STATO+", " +
//					F_TIPO_ORDINE+", " +
//					F_RIFERIMENTO_ORDINE+",  " +
//					F_STATO_CONFERMA+"  " +
//					" FROM "+ ORDINI + 
//					" WHERE " + F_TIPO_ORDINE + "=" +  "'E'" + " AND " + F_RIFERIMENTO_ORDINE + "=" + "'" + riferimento.trim() + "'" +
//					" AND " + F_ANNO + "* 10000 + "  + F_MESE + "*100 + " + F_GIORNO + " >= " +  getFromDate(subbedDays) + 
//					" AND " + F_ANNULLO + "<>" + "'A'";
			
			String select = "SELECT " +
					F_CLIENTE +", " +
					F_NUMERO+", " +
					F_ANNO+", " +
					F_MESE+", " +
					F_GIORNO+", " +
					F_GIRO+", " +
					F_STATO+", " +
					F_TIPO_ORDINE+", " +
					F_RIFERIMENTO_ORDINE+",  " +
					F_STATO_CONFERMA+"  " +
					" FROM "+ ORDINI + 
					" WHERE " + F_TIPO_ORDINE + "=" +  "'E'" + " AND " + F_RIFERIMENTO_ORDINE + " LIKE " + "'%" + riferimento.trim() + "%'" +
					" AND " + F_ANNULLO + "<>" + "'A'";
					  
	
			logger.info(TAG, select);
			
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
		 	
			while(rs.next()){
				found = true;
				break;
			}

			rs.close();

			logger.info(TAG, "Riferimento ordine S/N: " + found);
			return found;

		}

		catch(SQLException e){
			System.out.println(e);
			return false;
		}
	}

}
